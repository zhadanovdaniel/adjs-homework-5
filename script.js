class Card {
  constructor({ title, body, id, userId }, { name, email }) {
    this.postId = id;
    this.userId = userId;
    this.title = title;
    this.body = body;
    this.name = name;
    this.email = email;
  }
 
  render() {
    const postContainer = document.querySelector(".list");
    const card = document.createElement("div");
    
    card.classList.add("card");
    card.id = `${this.postId}`;
    card.innerHTML = `
      <h3>${this.title}</h3>
      <p>${this.body}</p>
      <span>Posted by: ${this.name}</span>
      <span>Author Email: ${this.email}</span>
      <button class="delete">delete</button>
    `;
    postContainer.append(card);

    // Select delete button after it has been appended to the DOM
    const deleteButton = card.querySelector(".delete");
    
    deleteButton.addEventListener("click", () => {
      this.deleteCard();
    });
  }

  deleteCard() {
    axios
      .delete(`https://ajax.test-danit.com/api/json/posts/${this.postId}`)
      .then(() => {
        let currentPost = document.getElementById(this.postId);
        if (currentPost) currentPost.remove();
      })
      .catch((error) => {
        console.log("Error deleting post:", error);
      });
  }
}

axios
  .get("https://ajax.test-danit.com/api/json/users")
  .then(({ data: userData }) => {
    axios
      .get("https://ajax.test-danit.com/api/json/posts")
      .then(({ data: postData }) => {
        postData.forEach((el) => {
          const userFind = userData.find((user) => user.id === el.userId);
          if (userFind) {
            new Card(el, userFind).render();
          }
        });
      })
      .catch((error) => {
        console.log("Error fetching posts:", error);
      });
  })
  .catch((error) => {
    console.log("Error fetching users:", error);
  });
